﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ooptest
{
    public partial class TownForm : Form
    {
        public TownForm()
        {
            InitializeComponent();
            UIHandler.TFLogBox = richLogBox;
        }
        private void healBtn_Click(object sender, EventArgs e)
        {
            if (UnitsHandler.Player.HP != UnitsHandler.Player.MaxHP && UnitsHandler.Player.Gold > 9)
            {
                UnitsHandler.Player.HP = UnitsHandler.Player.MaxHP;
                UnitsHandler.Player.Gold -= 10;
                RefreshUI();
            }
        }
        private void arenaBtn_Click(object sender, EventArgs e)
        {
            StartArenaLvlBtnsAnimation();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ar1Btn.Location.Y > 154) { ar1Btn.Location = new Point(ar1Btn.Location.X, ar1Btn.Location.Y - 1); }
            if (ar2Btn.Location.Y > 159) { ar2Btn.Location = new Point(ar2Btn.Location.X + 1, ar2Btn.Location.Y - 1); }
            if (ar3Btn.Location.X < 523) { ar3Btn.Location = new Point(ar3Btn.Location.X + 1, ar3Btn.Location.Y); }
            if (ar4Btn.Location.Y < 239) { ar4Btn.Location = new Point(ar4Btn.Location.X + 1, ar4Btn.Location.Y + 1); }
            if (ar5Btn.Location.Y < 243) { ar5Btn.Location = new Point(ar5Btn.Location.X, ar5Btn.Location.Y + 1); }
        }
        #region Arena levels buttons click
        private void ar1Btn_Click(object sender, EventArgs e)
        {
            DataStorage.ArenaLvl = 1;
            UIHandler.LoadScene(UIHandler.GameForm);
        }
        private void ar2Btn_Click(object sender, EventArgs e)
        {
            DataStorage.ArenaLvl = 2;
            UIHandler.LoadScene(UIHandler.GameForm);
        }
        private void ar3Btn_Click(object sender, EventArgs e)
        {
            DataStorage.ArenaLvl = 3;
            UIHandler.LoadScene(UIHandler.GameForm);
        }
        private void ar4Btn_Click(object sender, EventArgs e)
        {
            DataStorage.ArenaLvl = 4;
            UIHandler.LoadScene(UIHandler.GameForm);
        }
        private void ar5Btn_Click(object sender, EventArgs e)
        {
            DataStorage.ArenaLvl = 5;
            UIHandler.LoadScene(UIHandler.GameForm);
        }
        #endregion
        private void arenaBtnAreaLbl_MouseLeave(object sender, EventArgs e)
        {
            if (this.PointToClient(Cursor.Position).X > 620 || this.PointToClient(Cursor.Position).X < 400 || this.PointToClient(Cursor.Position).Y < 120 || this.PointToClient(Cursor.Position).Y > 300)
            {
                StopArenaLvlBtnsAnimation();
            }
        }
        #region Character info panel
        private void characterBtn_Click(object sender, EventArgs e)
        {
            if (charPanel.Visible == true) { charPanel.Visible = false; }
            else
            {
                RefreshCharPanel();
                charPanel.Visible = true;
            }
        }
        private void RefreshCharPanel()
        {
            cPNameLbl.Text = UnitsHandler.Player.Name;
            cPLvlLbl.Text = $"Lvl: {UnitsHandler.Player.Lvl.ToString()}";
            cPExpLbl.Text = $"XP: {UnitsHandler.Player.XPString}";
            cPHpLbl.Text = $"HP: {UnitsHandler.Player.HP}/{UnitsHandler.Player.MaxHP}";
            cPDmgLbl.Text = $"DMG: {UnitsHandler.Player.Dmg.ToString()}";
            cPDefLbl.Text = $"DEF: {UnitsHandler.Player.Def(3)}/{UnitsHandler.Player.Def(2)}/{UnitsHandler.Player.Def(1)}";
            cPPtsLbl.Text = $"Points left: {UnitsHandler.Player.Points.ToString()}";
            cPStrLbl.Text = $"STR: {UnitsHandler.Player.Str.ToString()}";
            cPAgiLbl.Text = $"AGI: {UnitsHandler.Player.Agi.ToString()}";
            cPConLbl.Text = $"CON: {UnitsHandler.Player.Con.ToString()}";
            if (UnitsHandler.Player.Points > 0) { cPAgiBtn.Enabled = true; cPStrBtn.Enabled = true; cPConBtn.Enabled = true; }
            else { cPAgiBtn.Enabled = false; cPStrBtn.Enabled = false; cPConBtn.Enabled = false; }
        }
        private void cPStrBtn_Click(object sender, EventArgs e)
        {
            UnitsHandler.Player.Str++;
            UnitsHandler.Player.Points--;
            RefreshCharPanel();
        }
        private void cPAgiBtn_Click(object sender, EventArgs e)
        {
            UnitsHandler.Player.Agi++;
            UnitsHandler.Player.Points--;
            RefreshCharPanel();
        }
        private void cPConBtn_Click(object sender, EventArgs e)
        {
            UnitsHandler.Player.Con++;
            UnitsHandler.Player.Points--;
            RefreshCharPanel();
            RefreshUI();
        }
        #endregion        
        public void RefreshUI()
        {
            hpLbl.Text = $"{UnitsHandler.Player.HP.ToString()}/{UnitsHandler.Player.MaxHP}";
            hpFrontPanel.Width = Convert.ToInt32(UnitsHandler.Player.HP * (Convert.ToDouble(200) / UnitsHandler.Player.MaxHP));
            lvlLbl.Text = UnitsHandler.Player.Lvl.ToString();
            expFrntPnl.Width = Convert.ToInt32(UnitsHandler.Player.XP * (Convert.ToDouble(200) / UnitsHandler.Player.XPneed));
            goldLbl.Text = $"Gold: {UnitsHandler.Player.Gold.ToString()}";
        }
        private void TownForm_VisibleChanged(object sender, EventArgs e)
        {
            RefreshUI();
            UIHandler.DisplayLog(UIHandler.TFLogBox, UIHandler.GFLogBox);
            StopArenaLvlBtnsAnimation();
            charPanel.Visible = false;
        }
        private void TownForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        private void StartArenaLvlBtnsAnimation()
        {
            arenaBtn.Enabled = false;
            timer1.Tick += timer1_Tick;
            timer1.Start();
        }
        private void StopArenaLvlBtnsAnimation()
        {
            timer1.Stop();
            timer1.Tick -= timer1_Tick;
            arenaBtn.Enabled = true;
            ar1Btn.Location = new Point(488, 189);
            ar2Btn.Location = new Point(488, 194);
            ar3Btn.Location = new Point(488, 199);
            ar4Btn.Location = new Point(488, 204);
            ar5Btn.Location = new Point(488, 209);
        }
    }
}
