﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ooptest
{
    public partial class ArenaForm : Form
    {
        public ArenaForm()
        {
            InitializeComponent();
            UIHandler.GFLogBox = this.richLogBox;
        }
        private void GameForm_Load(object sender, EventArgs e)
        {
            #region Add strikes/blocks into checkboxes
            foreach (Strike s in UnitsHandler.Player.Moves.KnownStrikes) { checkedListBox1.Items.Add(s); }
            foreach (Block b in UnitsHandler.Player.Moves.KnownBlocks) { checkedListBox2.Items.Add(b); }
            checkedListBox1.SetItemChecked(0, true);
            checkedListBox2.SetItemChecked(0, true);
            #endregion
        }
        private void GameForm_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                UnitsHandler.SpawnEnemy(DataStorage.ArenaLvl);
                RefreshUI();
            }
        }

        #region Checkboxes single choice implement
        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            attackBtn.Enabled = false;
            for (int i = 0; i < checkedListBox1.Items.Count; ++i)
                if (i != e.Index) checkedListBox1.SetItemChecked(i, false);
            if (e.NewValue == CheckState.Checked && checkedListBox2.CheckedItems.Count != 0)
            {
                attackBtn.Enabled = true;
            }
        }
        private void checkedListBox2_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            attackBtn.Enabled = false;
            for (int i = 0; i < checkedListBox2.Items.Count; ++i)
                if (i != e.Index) checkedListBox2.SetItemChecked(i, false);
            if (e.NewValue == CheckState.Checked && checkedListBox1.CheckedItems.Count != 0)
            {
                attackBtn.Enabled = true;
            }
        }
        #endregion

        private void attackBtn_Click(object sender, EventArgs e)
        {
            foreach (Strike chosen in checkedListBox1.CheckedItems) { UnitsHandler.Player.Moves.SelectStrike(chosen); }
            foreach (Block chosen in checkedListBox2.CheckedItems) { UnitsHandler.Player.Moves.SelectBlock(chosen); }
            UnitsHandler.Enemy.Moves.RandomStrike();
            UnitsHandler.Enemy.Moves.RandomBlock();
            UnitsHandler.Player.Attack(UnitsHandler.Enemy, out DataStorage.LatestLog);
            UIHandler.DisplayLog(this.richLogBox, DataStorage.LatestLog);
            if (UnitsHandler.Enemy.HP > 0)
            {
                UnitsHandler.Enemy.Attack(UnitsHandler.Player, out DataStorage.LatestLog);
                UIHandler.DisplayLog(richLogBox, DataStorage.LatestLog);
            }
            else
            {
                int gold = DataStorage.GetKillGoldBounty();
                int xp = DataStorage.GetKillXPBounty();
                UnitsHandler.Player.Gold += gold;
                UnitsHandler.Player.EarnXP(xp);
                DataStorage.UpdateDB();
                UIHandler.DisplayLog(this.richLogBox, $"{UnitsHandler.Enemy.Name} killed. XP: +{xp}. Gold: +{gold}.");
                UnitsHandler.SpawnEnemy(DataStorage.ArenaLvl);
            }            
            RefreshUI();
        }
        private void RefreshUI()
        {
            if (UnitsHandler.Enemy.HP == UnitsHandler.Enemy.MaxHP) leaveArenaBtn.Enabled = true; else leaveArenaBtn.Enabled = false;
            goldLbl.Text = $"Gold: {UnitsHandler.Player.Gold.ToString()}";
            lvlLbl.Text = UnitsHandler.Player.Lvl.ToString();
            expFrntPnl.Width = Convert.ToInt32(UnitsHandler.Player.XP * (Convert.ToDouble(200) / UnitsHandler.Player.XPneed));
            hpLbl.Text = $"{UnitsHandler.Player.HP}/{UnitsHandler.Player.MaxHP}";
            hpFrontPanel.Width = Convert.ToInt32(UnitsHandler.Player.HP * (Convert.ToDouble(200) / UnitsHandler.Player.MaxHP));
            enmNameLbl.Text = UnitsHandler.Enemy.Name;
            enmHpFrntPnl.Width = Convert.ToInt32(UnitsHandler.Enemy.HP * (Convert.ToDouble(150) / UnitsHandler.Enemy.MaxHP));
        }
        private void leaveArenaBtn_Click(object sender, EventArgs e)
        {
            UIHandler.LoadScene(UIHandler.TownForm);
        }
        private void enmNameLbl_MouseEnter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(enmNameLbl, $"HP:{UnitsHandler.Enemy.HP}/{UnitsHandler.Enemy.MaxHP}\nSTR: {UnitsHandler.Enemy.Str}\nAGI: {UnitsHandler.Enemy.Agi}\nCON: {UnitsHandler.Enemy.Con}");
        }
        private void GameForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        private void GameForm_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black, 2);
            Pen pen2 = new Pen(Color.Black, 1);
            e.Graphics.DrawLine(pen, 176, 180, 618, 180);
            e.Graphics.DrawLine(pen2, 256, 263, 256, 185);
            e.Graphics.DrawLine(pen2, 255, 187, 251, 200);
            e.Graphics.DrawLine(pen2, 257, 187, 261, 200);
            e.Graphics.DrawLine(pen2, 539, 263, 539, 185);
            e.Graphics.DrawLine(pen2, 538, 261, 534, 249);
            e.Graphics.DrawLine(pen2, 540, 261, 544, 249);
        }
    }
}
