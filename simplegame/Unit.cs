﻿using System;
using System.Collections.Generic;

namespace ooptest
{
    public class Unit
    {
        private readonly string name;
        private readonly int basehp, baseDmg;
        private int hp, str, agi, con;
        private CombatMoves moves;
        public Unit(string name, int hP, int dmg, int str, int agi, int con, List<Strike> strikes, List<Block> blocks)
        {
            this.name = name;
            hp = hP + (con * 10);
            basehp = hP;
            baseDmg = dmg;
            this.str = str; this.agi = agi; this.con = con;
            moves = new CombatMoves(strikes, blocks);
        }
        public string Name { get => name; }
        public int Str { get => str; set => str = value; }
        public int Agi { get => agi; set => agi = value; }
        public int Con { get => con; set => con = value; }
        public int Dmg { get => baseDmg + (str * 2); }
        public int HP { get => hp; set => hp = value; }
        public int MaxHP { get => basehp + (con * 10); }
        public CombatMoves Moves { get => moves; }
        public void Attack(Unit target, out string log)
        {
            int currentRoll = Dice.Next();
            int dmg;
            log = $"{this.Name} attack {target.Name} with {this.moves.SelectedStrike.ToString()}";
            if (this.moves.SelectedStrike.Level == target.moves.SelectedBlock.Level)//block case
            {
                if ((this.moves.SelectedStrike.Weight - target.moves.SelectedBlock.Weight + currentRoll) > 50)//block crushed
                {
                    int rand = Dice.Next(Convert.ToInt32(Math.Round(this.Dmg / 2 * 0.3)));
                    if (Dice.Next() > 50) rand = rand * -1;
                    dmg = (this.Dmg - target.Def(this.moves.SelectedStrike.Level)) / 2 + rand;
                    log += $". {target.Name} block attack, but {this.Name} crush the block and deal {dmg} damage!";
                }
                else//blocked
                {
                    dmg = 1;
                    log += $", but {target.Name} block it. {this.Name} deal {dmg} damage.";
                }
            }
            else//casual hit
            {
                int rand = Dice.Next(Convert.ToInt32(Math.Round(this.Dmg * 0.3)));
                if (Dice.Next() > 50) rand = rand * -1;
                dmg = this.Dmg - target.Def(this.moves.SelectedStrike.Level) + rand;
                log += $". Dealt {dmg} damage.";
            }
            if (dmg < 1) dmg = 1;

            target.TakeDamage(dmg);
        }
        public int Def(int level)
        {
            switch (level)
            {
                case 1:
                    return 0 + agi * 1;
                case 2:
                    return 0 + agi * 1;
                case 3:
                    return 0 + agi * 1;
                default:
                    return 0;
            }
        }
        public void TakeDamage(int val)
        {
            hp -= val;            
        }   
    }
    public class Player : Unit
    {
        private int level;
        private int experience;
        private int attPoints;
        private int playerGold;
        public Player(string name, int lvl, int exp, int attpts, int gold, int hP, int dmg, int str, int agi, int con, List<Strike> strikes, List<Block> blocks)
            : base(name, hP, dmg, str, agi, con, strikes, blocks)
        {
            level = lvl;
            experience = exp;
            attPoints = attpts;
            playerGold = gold;
        }
        public int Lvl { get => level; }
        public string XPString
        {
            get
            {
                int needXP = Convert.ToInt32((100 * Math.Pow(2, level)) / 2);
                return $"{experience}/{needXP}";
            }
        }
        public int XP { get => experience; }
        public int XPneed { get => Convert.ToInt32((100 * Math.Pow(2, level)) / 2); }
        public int Points { get => attPoints; set { attPoints = value; } }
        public int Gold { get { return playerGold; } set { playerGold = value; } }
        public void EarnXP(int amount)
        {
            experience += amount;
            if (experience > Convert.ToInt32((100 * Math.Pow(2, level)) / 2))
            {
                experience -= Convert.ToInt32((100 * Math.Pow(2, level)) / 2); level++;
                attPoints++;
            }
        }
    }
    public class CombatMoves
    {
        private List<Strike> knownStrikes;
        private List<Block> knownBlocks;
        private Strike selectedStrike;
        private Block selectedBlock;
        public CombatMoves(List<Strike> strikes, List<Block> blocks)
        {
            knownStrikes = strikes;
            knownBlocks = blocks;
        }
        public List<Strike> KnownStrikes { get => knownStrikes; }
        public List<Block> KnownBlocks { get => knownBlocks; }
        public Strike SelectedStrike { get => selectedStrike; }
        public Block SelectedBlock { get => selectedBlock; }
        public void SelectStrike(Strike strike)
        {
            selectedStrike = strike;
        }
        public void SelectBlock(Block block)
        {
            selectedBlock = block;
        }
        public void RandomStrike()
        {
            selectedStrike = knownStrikes[Dice.Next(knownStrikes.Count)];
        }
        public void RandomBlock()
        {
            selectedBlock = knownBlocks[Dice.Next(knownBlocks.Count)];
        }
    }
    public static class UnitsHandler
    {
        private static Player currentPlayer;
        private static Unit currentEnemy;
        public static Player Player { get { return currentPlayer; } set { currentPlayer = value; } }
        public static Unit Enemy { get { return currentEnemy; } set { currentEnemy = value; } }
        public static void SpawnEnemy(int lvl)
        {
            int points, hp = 30, dmg = 3, str = 0, agi = 0, con = 0;
            string name = "Enemy";
            points = lvl * 3;
            for (int i = 0; i < points; i++)
            {
                switch (Dice.Next())
                {
                    case int n when (n > 0 && n < 34):
                        str++;
                        break;
                    case int n when (n > 33 && n < 67):
                        agi++;
                        break;
                    case int n when (n > 66 && n < 101):
                        con++;
                        break;
                }
            }
            switch (lvl)
            {
                case 1:
                    name = "Cave Bat";
                    hp = 30;
                    dmg = 3;
                    break;
                case 2:
                    name = "Peltast";
                    hp = 60;
                    dmg = 7;
                    break;
                case 3:
                    name = "Hoplite";
                    hp = 80;
                    dmg = 10;
                    break;
                case 4:
                    name = "Centurion";
                    hp = 100;
                    dmg = 15;
                    break;
                case 5:
                    name = "Champion";
                    hp = 150;
                    dmg = 20;
                    break;
            }
            UnitsHandler.Enemy = new Unit(name, hp, dmg, str, agi, con, StrikePresets.Default, BlockPresets.Default);
        }
    }
}
