﻿using System.Collections.Generic;

namespace ooptest
{
    public abstract class Strike
    {
        private int weight;
        private int level;
        public Strike(int weight, int level)
        {
            this.weight = weight;
            this.level = level;
        }
        public int Level { get => level; }
        public int Weight { get => weight; }
    }
    public class Hook : Strike
    {
        public override string ToString()
        {
            return "Hook";
        }
        public Hook() : base(100, 3) { }
    }
    public class LiverPunch : Strike
    {
        public override string ToString()
        {
            return "Liver Punch";
        }
        public LiverPunch() : base(100, 2) { }
    }
    public class LowKick : Strike
    {
        public override string ToString()
        {
            return "Low Kick";
        }
        public LowKick() : base(100, 1) { }
    }
    public static class StrikePresets
    {
        public static readonly List<Strike> Default = new List<Strike> { new Hook(), new LiverPunch(), new LowKick() };
    }    
}
