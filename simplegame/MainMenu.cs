﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace ooptest
{
    public partial class MainMenu : Form
    {
        DB db = new DB();
        public MainMenu()
        {
            InitializeComponent();
            Dice.Randomizer = new Random();
        }
        private void StartMenu_Load(object sender, EventArgs e)
        {
            #region Leaderboard fill
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            MySqlCommand command = new MySqlCommand("SELECT name, level, experience FROM characters ORDER BY level DESC, experience DESC", db.GetConnection());
            adapter.SelectCommand = command;
            DataTable table = new DataTable();
            adapter.Fill(table);
            dataGridView1.DataSource = table;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridView1.AutoResizeColumns();
            dataGridView1.ClearSelection();
            #endregion
            UIHandler.CurrentForm = this;
            UIHandler.StartMenuForm = this;
            UIHandler.TownForm = new TownForm();
            UIHandler.TownForm.StartPosition = FormStartPosition.Manual;
            UIHandler.GameForm = new ArenaForm();
            UIHandler.GameForm.StartPosition = FormStartPosition.Manual;           
        }
        private void playBtn_Click(object sender, EventArgs e)
        {
            UIHandler.LoadScene(UIHandler.TownForm);
        }
        private void loginBtn_Click(object sender, EventArgs e)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            MySqlCommand command = new MySqlCommand("SELECT * FROM users WHERE login = @login AND password = @password", db.GetConnection());
            command.Parameters.Add("@login", MySqlDbType.VarChar).Value = loginBox.Text;
            command.Parameters.Add("@password", MySqlDbType.VarChar).Value = passBox.Text;
            adapter.SelectCommand = command;
            DataTable table = new DataTable();
            adapter.Fill(table);
            bool passCorrect = table.Rows.Count > 0;
            if (passCorrect)
            {
                loginLbl.Text = "Successfully login";
                createCharBtn.Visible = false;
                DataStorage.Nick = table.Rows[0][3].ToString();
                command = new MySqlCommand("SELECT * FROM characters WHERE name = @name", db.GetConnection());
                command.Parameters.Add("@name", MySqlDbType.VarChar).Value = table.Rows[0][3];
                adapter.SelectCommand = command;
                table = new DataTable();
                adapter.Fill(table);
                bool charExist = table.Rows.Count > 0;
                if (charExist)
                {
                    ArrayList arr = new ArrayList();
                    foreach (var item in table.Rows[0].ItemArray) { arr.Add(item); }
                    UnitsHandler.Player = new Player((string)arr[0], (int)arr[1], (int)arr[2], (int)arr[3], (int)arr[4], 100, (int)arr[6], (int)arr[7], (int)arr[8], (int)arr[9], StrikePresets.Default, BlockPresets.Default);
                    RefreshCharPanel();
                    charPanel.Visible = true;
                    playBtn.Enabled = true;
                }
                else
                {
                    createCharBtn.Visible = true;
                    charPanel.Visible = false;
                    playBtn.Enabled = false;
                }
            }
            else { loginLbl.Text = "Incorrect login/pass"; }
        }
        private void RefreshCharPanel()
        {
            cPNameLbl.Text = UnitsHandler.Player.Name;
            cPLvlLbl.Text = $"Lvl: {UnitsHandler.Player.Lvl.ToString()}";
            cPExpLbl.Text = $"XP: {UnitsHandler.Player.XPString}";
            cPHpLbl.Text = $"HP: {UnitsHandler.Player.HP}/{UnitsHandler.Player.MaxHP}";
            cPDmgLbl.Text = $"DMG: {UnitsHandler.Player.Dmg.ToString()}";
            cPDefLbl.Text = $"DEF: {UnitsHandler.Player.Def(3)}/{UnitsHandler.Player.Def(2)}/{UnitsHandler.Player.Def(1)}";
            cPStrLbl.Text = $"STR: {UnitsHandler.Player.Str.ToString()}";
            cPAgiLbl.Text = $"AGI: {UnitsHandler.Player.Agi.ToString()}";
            cPConLbl.Text = $"CON: {UnitsHandler.Player.Con.ToString()}";
        }
        private bool IfLoginAvailable()
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            MySqlCommand command = new MySqlCommand("SELECT login FROM users WHERE login = @login", db.GetConnection());
            command.Parameters.Add("@login", MySqlDbType.VarChar).Value = loginRegBox.Text;
            adapter.SelectCommand = command;
            DataTable table = new DataTable();
            adapter.Fill(table);
            if (table.Rows.Count > 0) { return false; }
            else { return true; }
        }
        private void regBtn_Click(object sender, EventArgs e)
        {
            if (loginRegBox.Text == "") { regLbl.Text = "Enter login"; return; }
            if (passRegBox.Text == "") { regLbl.Text = "Enter password"; return; }
            if (nameRegBox.Text == "") { regLbl.Text = "Enter nickname"; return; }
            if (!IfLoginAvailable()) { regLbl.Text = $"User {loginRegBox.Text} already exists"; return; }

            MySqlCommand command = new MySqlCommand("INSERT INTO `users` (`login`, `password`, `name`) VALUES (@login,@password,@name);", db.GetConnection());
            command.Parameters.Add("@login", MySqlDbType.VarChar).Value = loginRegBox.Text;
            command.Parameters.Add("@password", MySqlDbType.VarChar).Value = passRegBox.Text;
            command.Parameters.Add("@name", MySqlDbType.VarChar).Value = nameRegBox.Text;

            db.OpenConnection();
            if (command.ExecuteNonQuery() == 1)
            {
                loginLbl.Text = "Successfully registered, login with your account";
                registerPanel.Visible = false;
                loginPanel.Visible = true;
                showLoginBtn.Enabled = false;
                showRegBtn.Enabled = true;
            }
            else { regLbl.Text = "Error"; }
            db.CloseConnection();
        }
        private void showLoginBtn_Click(object sender, EventArgs e)
        {
            showLoginBtn.Enabled = false;
            loginLbl.Text = "";
            registerPanel.Visible = false;
            loginPanel.Visible = true;
            showRegBtn.Enabled = true;
        }
        private void showRegBtn_Click(object sender, EventArgs e)
        {
            showRegBtn.Enabled = false;
            regLbl.Text = "";
            registerPanel.Visible = true;
            loginPanel.Visible = false;
            showLoginBtn.Enabled = true;
        }
        private void createCharBtn_Click(object sender, EventArgs e)
        {
            createCharBtn.Visible = false;

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            MySqlCommand command = new MySqlCommand("INSERT INTO `characters` (`name`, `level`, `experience`, `attributepoints`, `gold`, `hp`, `dmg`, `str`, `agi`, `con`) VALUES (@name, '1', '0', '5', '0', '100', '10', '0', '0', '0');", db.GetConnection());
            command.Parameters.Add("@name", MySqlDbType.VarChar).Value = DataStorage.Nick;
            adapter.SelectCommand = command;
            db.OpenConnection();
            if (command.ExecuteNonQuery() == 1)
            {
                command = new MySqlCommand("SELECT * FROM characters WHERE name = @name", db.GetConnection());
                command.Parameters.Add("@name", MySqlDbType.VarChar).Value = DataStorage.Nick;
                adapter.SelectCommand = command;
                DataTable table = new DataTable();
                adapter.Fill(table);
                bool charExist = table.Rows.Count > 0;
                if (charExist)
                {
                    ArrayList arr = new ArrayList();
                    foreach (var item in table.Rows[0].ItemArray) { arr.Add(item); }
                    UnitsHandler.Player = new Player((string)arr[0], (int)arr[1], (int)arr[2], (int)arr[3], (int)arr[4], (int)arr[5], (int)arr[6], (int)arr[7], (int)arr[8], (int)arr[9], StrikePresets.Default, BlockPresets.Default);
                    RefreshCharPanel();
                    charPanel.Visible = true;
                    playBtn.Enabled = true;
                }
                else
                {
                    createCharBtn.Visible = true;
                }
            }
            else { createCharBtn.Visible = true; }
            db.CloseConnection();
            RefreshCharPanel();
            charPanel.Visible = true;
            playBtn.Enabled = true;
        }        
    }
}
