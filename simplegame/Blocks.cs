﻿using System.Collections.Generic;

namespace ooptest
{
    public abstract class Block
    {
        private int weight;
        private int level;
        public Block(int weight, int level)
        {
            this.weight = weight;
            this.level = level;
        }
        public int Level { get => level; }
        public int Weight { get => weight; }
    }
    public class UpperBlock : Block
    {
        public UpperBlock() : base(100, 3) { }
        public override string ToString() { return "Head Block"; }
    }
    public class MiddleBlock : Block
    {
        public MiddleBlock() : base(100, 2) { }
        public override string ToString() { return "Body Block"; }
    }
    public class LowerBlock : Block
    {
        public LowerBlock() : base(100, 1) { }
        public override string ToString() { return "Lower Block"; }
    }
    public static class BlockPresets
    {
        public static readonly List<Block> Default = new List<Block> { new UpperBlock(), new MiddleBlock(), new LowerBlock() };
    }
}
