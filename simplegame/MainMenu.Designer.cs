﻿
namespace ooptest
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playBtn = new System.Windows.Forms.Button();
            this.loginPanel = new System.Windows.Forms.Panel();
            this.loginLbl = new System.Windows.Forms.Label();
            this.loginBtn = new System.Windows.Forms.Button();
            this.loginPassLbl = new System.Windows.Forms.Label();
            this.loginLoginLbl = new System.Windows.Forms.Label();
            this.passBox = new System.Windows.Forms.TextBox();
            this.loginBox = new System.Windows.Forms.TextBox();
            this.registerPanel = new System.Windows.Forms.Panel();
            this.regNameLbl = new System.Windows.Forms.Label();
            this.regPassLbl = new System.Windows.Forms.Label();
            this.regLoginLbl = new System.Windows.Forms.Label();
            this.regLbl = new System.Windows.Forms.Label();
            this.regBtn = new System.Windows.Forms.Button();
            this.nameRegBox = new System.Windows.Forms.TextBox();
            this.passRegBox = new System.Windows.Forms.TextBox();
            this.loginRegBox = new System.Windows.Forms.TextBox();
            this.showLoginBtn = new System.Windows.Forms.Button();
            this.showRegBtn = new System.Windows.Forms.Button();
            this.charPanel = new System.Windows.Forms.Panel();
            this.cPDefLbl = new System.Windows.Forms.Label();
            this.cPDmgLbl = new System.Windows.Forms.Label();
            this.cPConLbl = new System.Windows.Forms.Label();
            this.cPAgiLbl = new System.Windows.Forms.Label();
            this.cPStrLbl = new System.Windows.Forms.Label();
            this.cPHpLbl = new System.Windows.Forms.Label();
            this.cPExpLbl = new System.Windows.Forms.Label();
            this.cPLvlLbl = new System.Windows.Forms.Label();
            this.cPNameLbl = new System.Windows.Forms.Label();
            this.createCharBtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.loginPanel.SuspendLayout();
            this.registerPanel.SuspendLayout();
            this.charPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // playBtn
            // 
            this.playBtn.Enabled = false;
            this.playBtn.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.playBtn.Location = new System.Drawing.Point(200, 333);
            this.playBtn.Name = "playBtn";
            this.playBtn.Size = new System.Drawing.Size(250, 50);
            this.playBtn.TabIndex = 0;
            this.playBtn.Text = "Play";
            this.playBtn.UseVisualStyleBackColor = true;
            this.playBtn.Click += new System.EventHandler(this.playBtn_Click);
            // 
            // loginPanel
            // 
            this.loginPanel.Controls.Add(this.loginLbl);
            this.loginPanel.Controls.Add(this.loginBtn);
            this.loginPanel.Controls.Add(this.loginPassLbl);
            this.loginPanel.Controls.Add(this.loginLoginLbl);
            this.loginPanel.Controls.Add(this.passBox);
            this.loginPanel.Controls.Add(this.loginBox);
            this.loginPanel.Location = new System.Drawing.Point(200, 20);
            this.loginPanel.Name = "loginPanel";
            this.loginPanel.Size = new System.Drawing.Size(250, 200);
            this.loginPanel.TabIndex = 3;
            // 
            // loginLbl
            // 
            this.loginLbl.Location = new System.Drawing.Point(5, 112);
            this.loginLbl.Margin = new System.Windows.Forms.Padding(5);
            this.loginLbl.Name = "loginLbl";
            this.loginLbl.Size = new System.Drawing.Size(240, 50);
            this.loginLbl.TabIndex = 4;
            this.loginLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // loginBtn
            // 
            this.loginBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.loginBtn.Location = new System.Drawing.Point(0, 170);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(250, 30);
            this.loginBtn.TabIndex = 2;
            this.loginBtn.Text = "Login";
            this.loginBtn.UseVisualStyleBackColor = true;
            this.loginBtn.Click += new System.EventHandler(this.loginBtn_Click);
            // 
            // loginPassLbl
            // 
            this.loginPassLbl.AutoSize = true;
            this.loginPassLbl.Location = new System.Drawing.Point(49, 83);
            this.loginPassLbl.Name = "loginPassLbl";
            this.loginPassLbl.Size = new System.Drawing.Size(88, 13);
            this.loginPassLbl.TabIndex = 5;
            this.loginPassLbl.Text = "Enter password:";
            // 
            // loginLoginLbl
            // 
            this.loginLoginLbl.AutoSize = true;
            this.loginLoginLbl.Location = new System.Drawing.Point(72, 51);
            this.loginLoginLbl.Name = "loginLoginLbl";
            this.loginLoginLbl.Size = new System.Drawing.Size(65, 13);
            this.loginLoginLbl.TabIndex = 5;
            this.loginLoginLbl.Text = "Enter login:";
            // 
            // passBox
            // 
            this.passBox.Location = new System.Drawing.Point(145, 80);
            this.passBox.Margin = new System.Windows.Forms.Padding(5);
            this.passBox.Name = "passBox";
            this.passBox.PasswordChar = '*';
            this.passBox.Size = new System.Drawing.Size(100, 22);
            this.passBox.TabIndex = 1;
            // 
            // loginBox
            // 
            this.loginBox.Location = new System.Drawing.Point(145, 48);
            this.loginBox.Margin = new System.Windows.Forms.Padding(5);
            this.loginBox.Name = "loginBox";
            this.loginBox.Size = new System.Drawing.Size(100, 22);
            this.loginBox.TabIndex = 0;
            // 
            // registerPanel
            // 
            this.registerPanel.Controls.Add(this.regNameLbl);
            this.registerPanel.Controls.Add(this.regPassLbl);
            this.registerPanel.Controls.Add(this.regLoginLbl);
            this.registerPanel.Controls.Add(this.regLbl);
            this.registerPanel.Controls.Add(this.regBtn);
            this.registerPanel.Controls.Add(this.nameRegBox);
            this.registerPanel.Controls.Add(this.passRegBox);
            this.registerPanel.Controls.Add(this.loginRegBox);
            this.registerPanel.Location = new System.Drawing.Point(200, 20);
            this.registerPanel.Name = "registerPanel";
            this.registerPanel.Size = new System.Drawing.Size(250, 200);
            this.registerPanel.TabIndex = 3;
            this.registerPanel.Visible = false;
            // 
            // regNameLbl
            // 
            this.regNameLbl.AutoSize = true;
            this.regNameLbl.Location = new System.Drawing.Point(78, 83);
            this.regNameLbl.Name = "regNameLbl";
            this.regNameLbl.Size = new System.Drawing.Size(59, 13);
            this.regNameLbl.TabIndex = 5;
            this.regNameLbl.Text = "Enter nick:";
            // 
            // regPassLbl
            // 
            this.regPassLbl.AutoSize = true;
            this.regPassLbl.Location = new System.Drawing.Point(49, 51);
            this.regPassLbl.Name = "regPassLbl";
            this.regPassLbl.Size = new System.Drawing.Size(88, 13);
            this.regPassLbl.TabIndex = 5;
            this.regPassLbl.Text = "Enter password:";
            // 
            // regLoginLbl
            // 
            this.regLoginLbl.AutoSize = true;
            this.regLoginLbl.Location = new System.Drawing.Point(72, 19);
            this.regLoginLbl.Name = "regLoginLbl";
            this.regLoginLbl.Size = new System.Drawing.Size(65, 13);
            this.regLoginLbl.TabIndex = 5;
            this.regLoginLbl.Text = "Enter login:";
            // 
            // regLbl
            // 
            this.regLbl.Location = new System.Drawing.Point(5, 112);
            this.regLbl.Margin = new System.Windows.Forms.Padding(5);
            this.regLbl.Name = "regLbl";
            this.regLbl.Size = new System.Drawing.Size(240, 50);
            this.regLbl.TabIndex = 4;
            this.regLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // regBtn
            // 
            this.regBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.regBtn.Location = new System.Drawing.Point(0, 170);
            this.regBtn.Name = "regBtn";
            this.regBtn.Size = new System.Drawing.Size(250, 30);
            this.regBtn.TabIndex = 3;
            this.regBtn.Text = "Register";
            this.regBtn.UseVisualStyleBackColor = true;
            this.regBtn.Click += new System.EventHandler(this.regBtn_Click);
            // 
            // nameRegBox
            // 
            this.nameRegBox.Location = new System.Drawing.Point(145, 80);
            this.nameRegBox.Margin = new System.Windows.Forms.Padding(5);
            this.nameRegBox.Name = "nameRegBox";
            this.nameRegBox.Size = new System.Drawing.Size(100, 22);
            this.nameRegBox.TabIndex = 2;
            // 
            // passRegBox
            // 
            this.passRegBox.Location = new System.Drawing.Point(145, 48);
            this.passRegBox.Margin = new System.Windows.Forms.Padding(5);
            this.passRegBox.Name = "passRegBox";
            this.passRegBox.PasswordChar = '*';
            this.passRegBox.Size = new System.Drawing.Size(100, 22);
            this.passRegBox.TabIndex = 1;
            // 
            // loginRegBox
            // 
            this.loginRegBox.Location = new System.Drawing.Point(145, 16);
            this.loginRegBox.Margin = new System.Windows.Forms.Padding(5);
            this.loginRegBox.Name = "loginRegBox";
            this.loginRegBox.Size = new System.Drawing.Size(100, 22);
            this.loginRegBox.TabIndex = 0;
            // 
            // showLoginBtn
            // 
            this.showLoginBtn.Enabled = false;
            this.showLoginBtn.Location = new System.Drawing.Point(0, 79);
            this.showLoginBtn.Name = "showLoginBtn";
            this.showLoginBtn.Size = new System.Drawing.Size(75, 23);
            this.showLoginBtn.TabIndex = 4;
            this.showLoginBtn.Text = "Login";
            this.showLoginBtn.UseVisualStyleBackColor = true;
            this.showLoginBtn.Click += new System.EventHandler(this.showLoginBtn_Click);
            // 
            // showRegBtn
            // 
            this.showRegBtn.Location = new System.Drawing.Point(0, 108);
            this.showRegBtn.Name = "showRegBtn";
            this.showRegBtn.Size = new System.Drawing.Size(75, 23);
            this.showRegBtn.TabIndex = 5;
            this.showRegBtn.Text = "Register";
            this.showRegBtn.UseVisualStyleBackColor = true;
            this.showRegBtn.Click += new System.EventHandler(this.showRegBtn_Click);
            // 
            // charPanel
            // 
            this.charPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.charPanel.Controls.Add(this.cPDefLbl);
            this.charPanel.Controls.Add(this.cPDmgLbl);
            this.charPanel.Controls.Add(this.cPConLbl);
            this.charPanel.Controls.Add(this.cPAgiLbl);
            this.charPanel.Controls.Add(this.cPStrLbl);
            this.charPanel.Controls.Add(this.cPHpLbl);
            this.charPanel.Controls.Add(this.cPExpLbl);
            this.charPanel.Controls.Add(this.cPLvlLbl);
            this.charPanel.Controls.Add(this.cPNameLbl);
            this.charPanel.Location = new System.Drawing.Point(12, 263);
            this.charPanel.Name = "charPanel";
            this.charPanel.Size = new System.Drawing.Size(115, 186);
            this.charPanel.TabIndex = 36;
            this.charPanel.Visible = false;
            // 
            // cPDefLbl
            // 
            this.cPDefLbl.Location = new System.Drawing.Point(5, 90);
            this.cPDefLbl.Name = "cPDefLbl";
            this.cPDefLbl.Size = new System.Drawing.Size(70, 14);
            this.cPDefLbl.TabIndex = 2;
            this.cPDefLbl.Text = "DEF: 99";
            // 
            // cPDmgLbl
            // 
            this.cPDmgLbl.Location = new System.Drawing.Point(5, 74);
            this.cPDmgLbl.Name = "cPDmgLbl";
            this.cPDmgLbl.Size = new System.Drawing.Size(70, 20);
            this.cPDmgLbl.TabIndex = 2;
            this.cPDmgLbl.Text = "DMG: 99";
            // 
            // cPConLbl
            // 
            this.cPConLbl.Location = new System.Drawing.Point(5, 160);
            this.cPConLbl.Name = "cPConLbl";
            this.cPConLbl.Size = new System.Drawing.Size(50, 15);
            this.cPConLbl.TabIndex = 2;
            this.cPConLbl.Text = "CON: 10";
            // 
            // cPAgiLbl
            // 
            this.cPAgiLbl.Location = new System.Drawing.Point(5, 136);
            this.cPAgiLbl.Name = "cPAgiLbl";
            this.cPAgiLbl.Size = new System.Drawing.Size(50, 20);
            this.cPAgiLbl.TabIndex = 2;
            this.cPAgiLbl.Text = "AGI: 10";
            // 
            // cPStrLbl
            // 
            this.cPStrLbl.Location = new System.Drawing.Point(5, 112);
            this.cPStrLbl.Name = "cPStrLbl";
            this.cPStrLbl.Size = new System.Drawing.Size(50, 20);
            this.cPStrLbl.TabIndex = 2;
            this.cPStrLbl.Text = "STR: 10";
            // 
            // cPHpLbl
            // 
            this.cPHpLbl.Location = new System.Drawing.Point(5, 58);
            this.cPHpLbl.Margin = new System.Windows.Forms.Padding(3);
            this.cPHpLbl.Name = "cPHpLbl";
            this.cPHpLbl.Size = new System.Drawing.Size(78, 20);
            this.cPHpLbl.TabIndex = 1;
            this.cPHpLbl.Text = "HP: 450/450";
            // 
            // cPExpLbl
            // 
            this.cPExpLbl.Location = new System.Drawing.Point(5, 35);
            this.cPExpLbl.Margin = new System.Windows.Forms.Padding(3);
            this.cPExpLbl.Name = "cPExpLbl";
            this.cPExpLbl.Size = new System.Drawing.Size(108, 20);
            this.cPExpLbl.TabIndex = 1;
            this.cPExpLbl.Text = "Exp: 25600/25600";
            // 
            // cPLvlLbl
            // 
            this.cPLvlLbl.Location = new System.Drawing.Point(5, 20);
            this.cPLvlLbl.Margin = new System.Windows.Forms.Padding(3);
            this.cPLvlLbl.Name = "cPLvlLbl";
            this.cPLvlLbl.Size = new System.Drawing.Size(72, 20);
            this.cPLvlLbl.TabIndex = 1;
            this.cPLvlLbl.Text = "Level: 10";
            // 
            // cPNameLbl
            // 
            this.cPNameLbl.Location = new System.Drawing.Point(5, 5);
            this.cPNameLbl.Name = "cPNameLbl";
            this.cPNameLbl.Size = new System.Drawing.Size(108, 14);
            this.cPNameLbl.TabIndex = 0;
            this.cPNameLbl.Text = "NameNameName";
            // 
            // createCharBtn
            // 
            this.createCharBtn.Location = new System.Drawing.Point(12, 262);
            this.createCharBtn.Name = "createCharBtn";
            this.createCharBtn.Size = new System.Drawing.Size(115, 53);
            this.createCharBtn.TabIndex = 37;
            this.createCharBtn.Text = "Create new character";
            this.createCharBtn.UseVisualStyleBackColor = true;
            this.createCharBtn.Visible = false;
            this.createCharBtn.Click += new System.EventHandler(this.createCharBtn_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(489, 79);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(263, 304);
            this.dataGridView1.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(486, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 32);
            this.label1.TabIndex = 39;
            this.label1.Text = "Leaderboard";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.createCharBtn);
            this.Controls.Add(this.charPanel);
            this.Controls.Add(this.showRegBtn);
            this.Controls.Add(this.showLoginBtn);
            this.Controls.Add(this.playBtn);
            this.Controls.Add(this.loginPanel);
            this.Controls.Add(this.registerPanel);
            this.DoubleBuffered = true;
            this.MaximumSize = new System.Drawing.Size(800, 500);
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "MainMenu";
            this.Text = "Game";
            this.Load += new System.EventHandler(this.StartMenu_Load);
            this.loginPanel.ResumeLayout(false);
            this.loginPanel.PerformLayout();
            this.registerPanel.ResumeLayout(false);
            this.registerPanel.PerformLayout();
            this.charPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button playBtn;
        private System.Windows.Forms.Panel loginPanel;
        private System.Windows.Forms.Button loginBtn;
        private System.Windows.Forms.TextBox passBox;
        private System.Windows.Forms.TextBox loginBox;
        private System.Windows.Forms.Panel registerPanel;
        private System.Windows.Forms.TextBox nameRegBox;
        private System.Windows.Forms.TextBox passRegBox;
        private System.Windows.Forms.TextBox loginRegBox;
        private System.Windows.Forms.Button regBtn;
        private System.Windows.Forms.Button showLoginBtn;
        private System.Windows.Forms.Button showRegBtn;
        private System.Windows.Forms.Label loginLbl;
        private System.Windows.Forms.Label regLbl;
        private System.Windows.Forms.Label loginPassLbl;
        private System.Windows.Forms.Label loginLoginLbl;
        private System.Windows.Forms.Label regNameLbl;
        private System.Windows.Forms.Label regPassLbl;
        private System.Windows.Forms.Label regLoginLbl;
        private System.Windows.Forms.Panel charPanel;
        private System.Windows.Forms.Label cPDefLbl;
        private System.Windows.Forms.Label cPDmgLbl;
        private System.Windows.Forms.Label cPConLbl;
        private System.Windows.Forms.Label cPAgiLbl;
        private System.Windows.Forms.Label cPStrLbl;
        private System.Windows.Forms.Label cPHpLbl;
        private System.Windows.Forms.Label cPExpLbl;
        private System.Windows.Forms.Label cPLvlLbl;
        private System.Windows.Forms.Label cPNameLbl;
        private System.Windows.Forms.Button createCharBtn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
    }
}