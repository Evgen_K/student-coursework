﻿
namespace ooptest
{
    partial class ArenaForm
    {
        
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.attackBtn = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.leaveArenaBtn = new System.Windows.Forms.Button();
            this.hpLbl = new System.Windows.Forms.Label();
            this.hpBackPanel = new System.Windows.Forms.Panel();
            this.hpFrontPanel = new System.Windows.Forms.Panel();
            this.lvlLbl = new System.Windows.Forms.Label();
            this.strikeLbl = new System.Windows.Forms.Label();
            this.blockLbl = new System.Windows.Forms.Label();
            this.enmHpBckPnl = new System.Windows.Forms.Panel();
            this.enmHpFrntPnl = new System.Windows.Forms.Panel();
            this.enmNameLbl = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.richLogBox = new System.Windows.Forms.RichTextBox();
            this.expBckPnl = new System.Windows.Forms.Panel();
            this.expFrntPnl = new System.Windows.Forms.Panel();
            this.goldLbl = new System.Windows.Forms.Label();
            this.hpBackPanel.SuspendLayout();
            this.enmHpBckPnl.SuspendLayout();
            this.expBckPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // attackBtn
            // 
            this.attackBtn.Location = new System.Drawing.Point(350, 200);
            this.attackBtn.Name = "attackBtn";
            this.attackBtn.Size = new System.Drawing.Size(100, 50);
            this.attackBtn.TabIndex = 0;
            this.attackBtn.Text = "Attack";
            this.attackBtn.UseVisualStyleBackColor = true;
            this.attackBtn.Click += new System.EventHandler(this.attackBtn_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(245, 297);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(150, 72);
            this.checkedListBox1.TabIndex = 4;
            this.checkedListBox1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            // 
            // checkedListBox2
            // 
            this.checkedListBox2.CheckOnClick = true;
            this.checkedListBox2.FormattingEnabled = true;
            this.checkedListBox2.Location = new System.Drawing.Point(401, 297);
            this.checkedListBox2.Name = "checkedListBox2";
            this.checkedListBox2.Size = new System.Drawing.Size(150, 72);
            this.checkedListBox2.TabIndex = 4;
            this.checkedListBox2.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox2_ItemCheck);
            // 
            // leaveArenaBtn
            // 
            this.leaveArenaBtn.Location = new System.Drawing.Point(12, 186);
            this.leaveArenaBtn.Name = "leaveArenaBtn";
            this.leaveArenaBtn.Size = new System.Drawing.Size(126, 23);
            this.leaveArenaBtn.TabIndex = 8;
            this.leaveArenaBtn.Text = "<- Leave Arena";
            this.leaveArenaBtn.UseVisualStyleBackColor = true;
            this.leaveArenaBtn.Click += new System.EventHandler(this.leaveArenaBtn_Click);
            // 
            // hpLbl
            // 
            this.hpLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hpLbl.BackColor = System.Drawing.Color.Transparent;
            this.hpLbl.ForeColor = System.Drawing.Color.Black;
            this.hpLbl.Location = new System.Drawing.Point(301, 378);
            this.hpLbl.Name = "hpLbl";
            this.hpLbl.Size = new System.Drawing.Size(200, 20);
            this.hpLbl.TabIndex = 15;
            this.hpLbl.Text = "100/100";
            this.hpLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hpBackPanel
            // 
            this.hpBackPanel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hpBackPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.hpBackPanel.Controls.Add(this.hpFrontPanel);
            this.hpBackPanel.Location = new System.Drawing.Point(301, 399);
            this.hpBackPanel.Name = "hpBackPanel";
            this.hpBackPanel.Size = new System.Drawing.Size(200, 40);
            this.hpBackPanel.TabIndex = 17;
            // 
            // hpFrontPanel
            // 
            this.hpFrontPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hpFrontPanel.BackColor = System.Drawing.Color.Firebrick;
            this.hpFrontPanel.Location = new System.Drawing.Point(0, 0);
            this.hpFrontPanel.Name = "hpFrontPanel";
            this.hpFrontPanel.Size = new System.Drawing.Size(200, 40);
            this.hpFrontPanel.TabIndex = 0;
            // 
            // lvlLbl
            // 
            this.lvlLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lvlLbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvlLbl.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvlLbl.Location = new System.Drawing.Point(245, 399);
            this.lvlLbl.Name = "lvlLbl";
            this.lvlLbl.Size = new System.Drawing.Size(50, 50);
            this.lvlLbl.TabIndex = 18;
            this.lvlLbl.Text = "1";
            this.lvlLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // strikeLbl
            // 
            this.strikeLbl.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.strikeLbl.Location = new System.Drawing.Point(245, 264);
            this.strikeLbl.Name = "strikeLbl";
            this.strikeLbl.Size = new System.Drawing.Size(150, 30);
            this.strikeLbl.TabIndex = 20;
            this.strikeLbl.Text = "Choose strike:";
            this.strikeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // blockLbl
            // 
            this.blockLbl.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.blockLbl.Location = new System.Drawing.Point(401, 264);
            this.blockLbl.Name = "blockLbl";
            this.blockLbl.Size = new System.Drawing.Size(150, 30);
            this.blockLbl.TabIndex = 21;
            this.blockLbl.Text = "Choose block:";
            this.blockLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // enmHpBckPnl
            // 
            this.enmHpBckPnl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.enmHpBckPnl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.enmHpBckPnl.Controls.Add(this.enmHpFrntPnl);
            this.enmHpBckPnl.Location = new System.Drawing.Point(325, 59);
            this.enmHpBckPnl.Name = "enmHpBckPnl";
            this.enmHpBckPnl.Size = new System.Drawing.Size(150, 30);
            this.enmHpBckPnl.TabIndex = 22;
            // 
            // enmHpFrntPnl
            // 
            this.enmHpFrntPnl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.enmHpFrntPnl.BackColor = System.Drawing.Color.Firebrick;
            this.enmHpFrntPnl.Location = new System.Drawing.Point(0, 0);
            this.enmHpFrntPnl.Name = "enmHpFrntPnl";
            this.enmHpFrntPnl.Size = new System.Drawing.Size(150, 30);
            this.enmHpFrntPnl.TabIndex = 0;
            // 
            // enmNameLbl
            // 
            this.enmNameLbl.Location = new System.Drawing.Point(325, 33);
            this.enmNameLbl.Name = "enmNameLbl";
            this.enmNameLbl.Size = new System.Drawing.Size(150, 23);
            this.enmNameLbl.TabIndex = 23;
            this.enmNameLbl.Text = "Enemy";
            this.enmNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.enmNameLbl.MouseEnter += new System.EventHandler(this.enmNameLbl_MouseEnter);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 300;
            this.toolTip1.ReshowDelay = 100;
            // 
            // richLogBox
            // 
            this.richLogBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richLogBox.Location = new System.Drawing.Point(12, 349);
            this.richLogBox.Name = "richLogBox";
            this.richLogBox.ReadOnly = true;
            this.richLogBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richLogBox.Size = new System.Drawing.Size(220, 100);
            this.richLogBox.TabIndex = 26;
            this.richLogBox.Text = "";
            // 
            // expBckPnl
            // 
            this.expBckPnl.BackColor = System.Drawing.Color.LightGray;
            this.expBckPnl.Controls.Add(this.expFrntPnl);
            this.expBckPnl.Location = new System.Drawing.Point(301, 439);
            this.expBckPnl.Name = "expBckPnl";
            this.expBckPnl.Size = new System.Drawing.Size(200, 10);
            this.expBckPnl.TabIndex = 27;
            // 
            // expFrntPnl
            // 
            this.expFrntPnl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.expFrntPnl.BackColor = System.Drawing.Color.DarkGray;
            this.expFrntPnl.Location = new System.Drawing.Point(0, 0);
            this.expFrntPnl.Name = "expFrntPnl";
            this.expFrntPnl.Size = new System.Drawing.Size(100, 10);
            this.expFrntPnl.TabIndex = 28;
            // 
            // goldLbl
            // 
            this.goldLbl.AutoSize = true;
            this.goldLbl.Location = new System.Drawing.Point(12, 333);
            this.goldLbl.Name = "goldLbl";
            this.goldLbl.Size = new System.Drawing.Size(44, 13);
            this.goldLbl.TabIndex = 37;
            this.goldLbl.Text = "Gold: 0";
            // 
            // ArenaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.goldLbl);
            this.Controls.Add(this.expBckPnl);
            this.Controls.Add(this.richLogBox);
            this.Controls.Add(this.enmNameLbl);
            this.Controls.Add(this.enmHpBckPnl);
            this.Controls.Add(this.blockLbl);
            this.Controls.Add(this.strikeLbl);
            this.Controls.Add(this.lvlLbl);
            this.Controls.Add(this.hpBackPanel);
            this.Controls.Add(this.hpLbl);
            this.Controls.Add(this.leaveArenaBtn);
            this.Controls.Add(this.checkedListBox2);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.attackBtn);
            this.DoubleBuffered = true;
            this.MaximumSize = new System.Drawing.Size(800, 500);
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "ArenaForm";
            this.Text = "Game";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GameForm_FormClosed);
            this.Load += new System.EventHandler(this.GameForm_Load);
            this.VisibleChanged += new System.EventHandler(this.GameForm_VisibleChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.GameForm_Paint);
            this.hpBackPanel.ResumeLayout(false);
            this.enmHpBckPnl.ResumeLayout(false);
            this.expBckPnl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button attackBtn;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.CheckedListBox checkedListBox2;
        private System.Windows.Forms.Button leaveArenaBtn;
        private System.Windows.Forms.Label hpLbl;
        private System.Windows.Forms.Panel hpBackPanel;
        private System.Windows.Forms.Panel hpFrontPanel;
        private System.Windows.Forms.Label lvlLbl;
        private System.Windows.Forms.Label strikeLbl;
        private System.Windows.Forms.Label blockLbl;
        private System.Windows.Forms.Panel enmHpBckPnl;
        private System.Windows.Forms.Panel enmHpFrntPnl;
        private System.Windows.Forms.Label enmNameLbl;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.RichTextBox richLogBox;
        private System.Windows.Forms.Panel expBckPnl;
        private System.Windows.Forms.Panel expFrntPnl;
        private System.Windows.Forms.Label goldLbl;
    }
}

