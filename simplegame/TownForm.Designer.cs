﻿
namespace ooptest
{
    partial class TownForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.arenaBtn = new System.Windows.Forms.Button();
            this.hpBackPanel = new System.Windows.Forms.Panel();
            this.hpFrontPanel = new System.Windows.Forms.Panel();
            this.healBtn = new System.Windows.Forms.Button();
            this.lvlLbl = new System.Windows.Forms.Label();
            this.characterBtn = new System.Windows.Forms.Button();
            this.richLogBox = new System.Windows.Forms.RichTextBox();
            this.ar1Btn = new System.Windows.Forms.Button();
            this.ar2Btn = new System.Windows.Forms.Button();
            this.ar3Btn = new System.Windows.Forms.Button();
            this.ar4Btn = new System.Windows.Forms.Button();
            this.ar5Btn = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.arenaBtnAreaLbl = new System.Windows.Forms.Label();
            this.hpLbl = new System.Windows.Forms.Label();
            this.expFrntPnl = new System.Windows.Forms.Panel();
            this.expBckPnl = new System.Windows.Forms.Panel();
            this.charPanel = new System.Windows.Forms.Panel();
            this.cPAgiBtn = new System.Windows.Forms.Button();
            this.cPConBtn = new System.Windows.Forms.Button();
            this.cPStrBtn = new System.Windows.Forms.Button();
            this.cPDefLbl = new System.Windows.Forms.Label();
            this.cPDmgLbl = new System.Windows.Forms.Label();
            this.cPConLbl = new System.Windows.Forms.Label();
            this.cPAgiLbl = new System.Windows.Forms.Label();
            this.cPPtsLbl = new System.Windows.Forms.Label();
            this.cPStrLbl = new System.Windows.Forms.Label();
            this.cPHpLbl = new System.Windows.Forms.Label();
            this.cPExpLbl = new System.Windows.Forms.Label();
            this.cPLvlLbl = new System.Windows.Forms.Label();
            this.cPNameLbl = new System.Windows.Forms.Label();
            this.goldLbl = new System.Windows.Forms.Label();
            this.hpBackPanel.SuspendLayout();
            this.expBckPnl.SuspendLayout();
            this.charPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // arenaBtn
            // 
            this.arenaBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.arenaBtn.Location = new System.Drawing.Point(418, 189);
            this.arenaBtn.Name = "arenaBtn";
            this.arenaBtn.Size = new System.Drawing.Size(100, 50);
            this.arenaBtn.TabIndex = 0;
            this.arenaBtn.Text = "Enter Arena";
            this.arenaBtn.UseVisualStyleBackColor = true;
            this.arenaBtn.Click += new System.EventHandler(this.arenaBtn_Click);
            // 
            // hpBackPanel
            // 
            this.hpBackPanel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hpBackPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.hpBackPanel.Controls.Add(this.hpFrontPanel);
            this.hpBackPanel.Location = new System.Drawing.Point(301, 399);
            this.hpBackPanel.Name = "hpBackPanel";
            this.hpBackPanel.Size = new System.Drawing.Size(200, 40);
            this.hpBackPanel.TabIndex = 9;
            // 
            // hpFrontPanel
            // 
            this.hpFrontPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hpFrontPanel.BackColor = System.Drawing.Color.Firebrick;
            this.hpFrontPanel.Location = new System.Drawing.Point(0, 0);
            this.hpFrontPanel.Name = "hpFrontPanel";
            this.hpFrontPanel.Size = new System.Drawing.Size(200, 40);
            this.hpFrontPanel.TabIndex = 0;
            // 
            // healBtn
            // 
            this.healBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.healBtn.Location = new System.Drawing.Point(269, 189);
            this.healBtn.Name = "healBtn";
            this.healBtn.Size = new System.Drawing.Size(100, 50);
            this.healBtn.TabIndex = 12;
            this.healBtn.Text = "Heal (10 g)";
            this.healBtn.UseVisualStyleBackColor = true;
            this.healBtn.Click += new System.EventHandler(this.healBtn_Click);
            // 
            // lvlLbl
            // 
            this.lvlLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lvlLbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvlLbl.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvlLbl.Location = new System.Drawing.Point(245, 399);
            this.lvlLbl.Name = "lvlLbl";
            this.lvlLbl.Size = new System.Drawing.Size(50, 50);
            this.lvlLbl.TabIndex = 13;
            this.lvlLbl.Text = "1";
            this.lvlLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // characterBtn
            // 
            this.characterBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.characterBtn.Location = new System.Drawing.Point(569, 419);
            this.characterBtn.Name = "characterBtn";
            this.characterBtn.Size = new System.Drawing.Size(100, 30);
            this.characterBtn.TabIndex = 14;
            this.characterBtn.Text = "Character";
            this.characterBtn.UseVisualStyleBackColor = true;
            this.characterBtn.Click += new System.EventHandler(this.characterBtn_Click);
            // 
            // richLogBox
            // 
            this.richLogBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richLogBox.Location = new System.Drawing.Point(12, 349);
            this.richLogBox.Name = "richLogBox";
            this.richLogBox.ReadOnly = true;
            this.richLogBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richLogBox.Size = new System.Drawing.Size(220, 100);
            this.richLogBox.TabIndex = 27;
            this.richLogBox.Text = "";
            // 
            // ar1Btn
            // 
            this.ar1Btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ar1Btn.Location = new System.Drawing.Point(488, 189);
            this.ar1Btn.Name = "ar1Btn";
            this.ar1Btn.Size = new System.Drawing.Size(30, 30);
            this.ar1Btn.TabIndex = 28;
            this.ar1Btn.Text = "1";
            this.ar1Btn.UseVisualStyleBackColor = true;
            this.ar1Btn.Click += new System.EventHandler(this.ar1Btn_Click);
            // 
            // ar2Btn
            // 
            this.ar2Btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ar2Btn.Location = new System.Drawing.Point(488, 194);
            this.ar2Btn.Name = "ar2Btn";
            this.ar2Btn.Size = new System.Drawing.Size(30, 30);
            this.ar2Btn.TabIndex = 29;
            this.ar2Btn.Text = "2";
            this.ar2Btn.UseVisualStyleBackColor = true;
            this.ar2Btn.Click += new System.EventHandler(this.ar2Btn_Click);
            // 
            // ar3Btn
            // 
            this.ar3Btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ar3Btn.Location = new System.Drawing.Point(488, 199);
            this.ar3Btn.Name = "ar3Btn";
            this.ar3Btn.Size = new System.Drawing.Size(30, 30);
            this.ar3Btn.TabIndex = 30;
            this.ar3Btn.Text = "3";
            this.ar3Btn.UseVisualStyleBackColor = true;
            this.ar3Btn.Click += new System.EventHandler(this.ar3Btn_Click);
            // 
            // ar4Btn
            // 
            this.ar4Btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ar4Btn.Location = new System.Drawing.Point(488, 204);
            this.ar4Btn.Name = "ar4Btn";
            this.ar4Btn.Size = new System.Drawing.Size(30, 30);
            this.ar4Btn.TabIndex = 31;
            this.ar4Btn.Text = "4";
            this.ar4Btn.UseVisualStyleBackColor = true;
            this.ar4Btn.Click += new System.EventHandler(this.ar4Btn_Click);
            // 
            // ar5Btn
            // 
            this.ar5Btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ar5Btn.Location = new System.Drawing.Point(488, 209);
            this.ar5Btn.Name = "ar5Btn";
            this.ar5Btn.Size = new System.Drawing.Size(30, 30);
            this.ar5Btn.TabIndex = 32;
            this.ar5Btn.Text = "5";
            this.ar5Btn.UseVisualStyleBackColor = true;
            this.ar5Btn.Click += new System.EventHandler(this.ar5Btn_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // arenaBtnAreaLbl
            // 
            this.arenaBtnAreaLbl.BackColor = System.Drawing.Color.Transparent;
            this.arenaBtnAreaLbl.Location = new System.Drawing.Point(392, 121);
            this.arenaBtnAreaLbl.Name = "arenaBtnAreaLbl";
            this.arenaBtnAreaLbl.Size = new System.Drawing.Size(230, 190);
            this.arenaBtnAreaLbl.TabIndex = 33;
            this.arenaBtnAreaLbl.MouseLeave += new System.EventHandler(this.arenaBtnAreaLbl_MouseLeave);
            // 
            // hpLbl
            // 
            this.hpLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hpLbl.BackColor = System.Drawing.Color.Transparent;
            this.hpLbl.ForeColor = System.Drawing.Color.Black;
            this.hpLbl.Location = new System.Drawing.Point(301, 378);
            this.hpLbl.Name = "hpLbl";
            this.hpLbl.Size = new System.Drawing.Size(200, 20);
            this.hpLbl.TabIndex = 34;
            this.hpLbl.Text = "100/100";
            this.hpLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // expFrntPnl
            // 
            this.expFrntPnl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.expFrntPnl.BackColor = System.Drawing.Color.DarkGray;
            this.expFrntPnl.Location = new System.Drawing.Point(0, 0);
            this.expFrntPnl.Name = "expFrntPnl";
            this.expFrntPnl.Size = new System.Drawing.Size(100, 10);
            this.expFrntPnl.TabIndex = 28;
            // 
            // expBckPnl
            // 
            this.expBckPnl.BackColor = System.Drawing.Color.LightGray;
            this.expBckPnl.Controls.Add(this.expFrntPnl);
            this.expBckPnl.Location = new System.Drawing.Point(301, 439);
            this.expBckPnl.Name = "expBckPnl";
            this.expBckPnl.Size = new System.Drawing.Size(200, 10);
            this.expBckPnl.TabIndex = 28;
            // 
            // charPanel
            // 
            this.charPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.charPanel.Controls.Add(this.cPAgiBtn);
            this.charPanel.Controls.Add(this.cPConBtn);
            this.charPanel.Controls.Add(this.cPStrBtn);
            this.charPanel.Controls.Add(this.cPDefLbl);
            this.charPanel.Controls.Add(this.cPDmgLbl);
            this.charPanel.Controls.Add(this.cPConLbl);
            this.charPanel.Controls.Add(this.cPAgiLbl);
            this.charPanel.Controls.Add(this.cPPtsLbl);
            this.charPanel.Controls.Add(this.cPStrLbl);
            this.charPanel.Controls.Add(this.cPHpLbl);
            this.charPanel.Controls.Add(this.cPExpLbl);
            this.charPanel.Controls.Add(this.cPLvlLbl);
            this.charPanel.Controls.Add(this.cPNameLbl);
            this.charPanel.Location = new System.Drawing.Point(569, 287);
            this.charPanel.Name = "charPanel";
            this.charPanel.Size = new System.Drawing.Size(200, 111);
            this.charPanel.TabIndex = 35;
            this.charPanel.Visible = false;
            // 
            // cPAgiBtn
            // 
            this.cPAgiBtn.Location = new System.Drawing.Point(163, 56);
            this.cPAgiBtn.Name = "cPAgiBtn";
            this.cPAgiBtn.Size = new System.Drawing.Size(25, 25);
            this.cPAgiBtn.TabIndex = 36;
            this.cPAgiBtn.Text = "+";
            this.cPAgiBtn.UseVisualStyleBackColor = true;
            this.cPAgiBtn.Click += new System.EventHandler(this.cPAgiBtn_Click);
            // 
            // cPConBtn
            // 
            this.cPConBtn.Location = new System.Drawing.Point(163, 80);
            this.cPConBtn.Name = "cPConBtn";
            this.cPConBtn.Size = new System.Drawing.Size(25, 25);
            this.cPConBtn.TabIndex = 36;
            this.cPConBtn.Text = "+";
            this.cPConBtn.UseVisualStyleBackColor = true;
            this.cPConBtn.Click += new System.EventHandler(this.cPConBtn_Click);
            // 
            // cPStrBtn
            // 
            this.cPStrBtn.Location = new System.Drawing.Point(163, 32);
            this.cPStrBtn.Name = "cPStrBtn";
            this.cPStrBtn.Size = new System.Drawing.Size(25, 25);
            this.cPStrBtn.TabIndex = 36;
            this.cPStrBtn.Text = "+";
            this.cPStrBtn.UseVisualStyleBackColor = true;
            this.cPStrBtn.Click += new System.EventHandler(this.cPStrBtn_Click);
            // 
            // cPDefLbl
            // 
            this.cPDefLbl.Location = new System.Drawing.Point(5, 90);
            this.cPDefLbl.Name = "cPDefLbl";
            this.cPDefLbl.Size = new System.Drawing.Size(70, 14);
            this.cPDefLbl.TabIndex = 2;
            this.cPDefLbl.Text = "DEF: 99";
            // 
            // cPDmgLbl
            // 
            this.cPDmgLbl.Location = new System.Drawing.Point(5, 74);
            this.cPDmgLbl.Name = "cPDmgLbl";
            this.cPDmgLbl.Size = new System.Drawing.Size(70, 20);
            this.cPDmgLbl.TabIndex = 2;
            this.cPDmgLbl.Text = "DMG: 99";
            // 
            // cPConLbl
            // 
            this.cPConLbl.Location = new System.Drawing.Point(113, 86);
            this.cPConLbl.Name = "cPConLbl";
            this.cPConLbl.Size = new System.Drawing.Size(50, 15);
            this.cPConLbl.TabIndex = 2;
            this.cPConLbl.Text = "CON: 10";
            // 
            // cPAgiLbl
            // 
            this.cPAgiLbl.Location = new System.Drawing.Point(113, 62);
            this.cPAgiLbl.Name = "cPAgiLbl";
            this.cPAgiLbl.Size = new System.Drawing.Size(50, 20);
            this.cPAgiLbl.TabIndex = 2;
            this.cPAgiLbl.Text = "AGI: 10";
            // 
            // cPPtsLbl
            // 
            this.cPPtsLbl.Location = new System.Drawing.Point(111, 8);
            this.cPPtsLbl.Name = "cPPtsLbl";
            this.cPPtsLbl.Size = new System.Drawing.Size(80, 20);
            this.cPPtsLbl.TabIndex = 2;
            this.cPPtsLbl.Text = "Points left: 99";
            // 
            // cPStrLbl
            // 
            this.cPStrLbl.Location = new System.Drawing.Point(113, 38);
            this.cPStrLbl.Name = "cPStrLbl";
            this.cPStrLbl.Size = new System.Drawing.Size(50, 20);
            this.cPStrLbl.TabIndex = 2;
            this.cPStrLbl.Text = "STR: 10";
            // 
            // cPHpLbl
            // 
            this.cPHpLbl.Location = new System.Drawing.Point(5, 58);
            this.cPHpLbl.Margin = new System.Windows.Forms.Padding(3);
            this.cPHpLbl.Name = "cPHpLbl";
            this.cPHpLbl.Size = new System.Drawing.Size(78, 20);
            this.cPHpLbl.TabIndex = 1;
            this.cPHpLbl.Text = "HP: 450/450";
            // 
            // cPExpLbl
            // 
            this.cPExpLbl.Location = new System.Drawing.Point(5, 35);
            this.cPExpLbl.Margin = new System.Windows.Forms.Padding(3);
            this.cPExpLbl.Name = "cPExpLbl";
            this.cPExpLbl.Size = new System.Drawing.Size(108, 20);
            this.cPExpLbl.TabIndex = 1;
            this.cPExpLbl.Text = "Exp: 25600/25600";
            // 
            // cPLvlLbl
            // 
            this.cPLvlLbl.Location = new System.Drawing.Point(5, 20);
            this.cPLvlLbl.Margin = new System.Windows.Forms.Padding(3);
            this.cPLvlLbl.Name = "cPLvlLbl";
            this.cPLvlLbl.Size = new System.Drawing.Size(72, 20);
            this.cPLvlLbl.TabIndex = 1;
            this.cPLvlLbl.Text = "Level: 10";
            // 
            // cPNameLbl
            // 
            this.cPNameLbl.Location = new System.Drawing.Point(5, 5);
            this.cPNameLbl.Name = "cPNameLbl";
            this.cPNameLbl.Size = new System.Drawing.Size(108, 14);
            this.cPNameLbl.TabIndex = 0;
            this.cPNameLbl.Text = "NameNameName";
            // 
            // goldLbl
            // 
            this.goldLbl.AutoSize = true;
            this.goldLbl.Location = new System.Drawing.Point(12, 333);
            this.goldLbl.Name = "goldLbl";
            this.goldLbl.Size = new System.Drawing.Size(44, 13);
            this.goldLbl.TabIndex = 36;
            this.goldLbl.Text = "Gold: 0";
            // 
            // TownForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.goldLbl);
            this.Controls.Add(this.charPanel);
            this.Controls.Add(this.expBckPnl);
            this.Controls.Add(this.hpLbl);
            this.Controls.Add(this.arenaBtn);
            this.Controls.Add(this.ar5Btn);
            this.Controls.Add(this.ar4Btn);
            this.Controls.Add(this.ar3Btn);
            this.Controls.Add(this.ar2Btn);
            this.Controls.Add(this.ar1Btn);
            this.Controls.Add(this.richLogBox);
            this.Controls.Add(this.characterBtn);
            this.Controls.Add(this.lvlLbl);
            this.Controls.Add(this.healBtn);
            this.Controls.Add(this.hpBackPanel);
            this.Controls.Add(this.arenaBtnAreaLbl);
            this.DoubleBuffered = true;
            this.MaximumSize = new System.Drawing.Size(800, 500);
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "TownForm";
            this.Text = "Game";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TownForm_FormClosed);
            this.VisibleChanged += new System.EventHandler(this.TownForm_VisibleChanged);
            this.hpBackPanel.ResumeLayout(false);
            this.expBckPnl.ResumeLayout(false);
            this.charPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button arenaBtn;
        private System.Windows.Forms.Panel hpBackPanel;
        private System.Windows.Forms.Panel hpFrontPanel;
        private System.Windows.Forms.Button healBtn;
        private System.Windows.Forms.Label lvlLbl;
        private System.Windows.Forms.Button characterBtn;
        private System.Windows.Forms.RichTextBox richLogBox;
        private System.Windows.Forms.Button ar1Btn;
        private System.Windows.Forms.Button ar2Btn;
        private System.Windows.Forms.Button ar3Btn;
        private System.Windows.Forms.Button ar4Btn;
        private System.Windows.Forms.Button ar5Btn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label arenaBtnAreaLbl;
        private System.Windows.Forms.Label hpLbl;
        private System.Windows.Forms.Panel expFrntPnl;
        private System.Windows.Forms.Panel expBckPnl;
        private System.Windows.Forms.Panel charPanel;
        private System.Windows.Forms.Label cPHpLbl;
        private System.Windows.Forms.Label cPExpLbl;
        private System.Windows.Forms.Label cPLvlLbl;
        private System.Windows.Forms.Label cPNameLbl;
        private System.Windows.Forms.Button cPAgiBtn;
        private System.Windows.Forms.Button cPConBtn;
        private System.Windows.Forms.Button cPStrBtn;
        private System.Windows.Forms.Label cPDefLbl;
        private System.Windows.Forms.Label cPDmgLbl;
        private System.Windows.Forms.Label cPConLbl;
        private System.Windows.Forms.Label cPAgiLbl;
        private System.Windows.Forms.Label cPPtsLbl;
        private System.Windows.Forms.Label cPStrLbl;
        private System.Windows.Forms.Label goldLbl;
    }
}