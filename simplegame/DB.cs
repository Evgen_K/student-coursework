﻿using MySql.Data.MySqlClient;

namespace ooptest
{
    class DB
    {
        MySqlConnection connection = new MySqlConnection("server=37.140.192.16;database=u1286497_testdb;port=3306;username=u1286497_testusr;password=thepassword;");
        public void OpenConnection()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
            { connection.Open(); }
        }
        public void CloseConnection()
        {
            if (connection.State == System.Data.ConnectionState.Open)
            { connection.Close(); }
        }
        public MySqlConnection GetConnection()
        {
            return connection;
        }
    }
}
