﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ooptest
{
    public static class Dice
    {
        public static Random Randomizer { get; set; }
        public static int Next(int max) { return Randomizer.Next(max); }
        public static int Next() { return Randomizer.Next(1, 101); }        
    }
    public static class UIHandler
    {
        public static RichTextBox GFLogBox { get; set; }
        public static RichTextBox TFLogBox { get; set; }
        public static Form CurrentForm { get; set; }
        public static Form StartMenuForm { get; set; }
        public static Form TownForm { get; set; }
        public static Form GameForm { get; set; }
        public static void DisplayLog(RichTextBox targetBox, string logText)
        {
            targetBox.Text = logText + Environment.NewLine + targetBox.Text;
            string[] abs = new string[] { };
            abs = targetBox.Text.Split('\n');
            int pos = 0;
            for (int i = 0; i < abs.Count(); i++)
            {
                targetBox.SelectionStart = pos;
                targetBox.SelectionLength = abs[i].Length + 1;
                if (i % 2 == 0) { targetBox.SelectionBackColor = Color.FromArgb(246, 233, 209); }
                else { targetBox.SelectionBackColor = Color.FromArgb(216, 203, 177); }
                if (abs[i].Contains("killed")) { targetBox.SelectionBackColor = Color.FromArgb(110, 255, 90); }
                pos += abs[i].Length + 1;
            }
        }
        public static void DisplayLog(RichTextBox toBox, RichTextBox fromBox)
        {
            toBox.Text = fromBox.Text;
            string[] abs = new string[] { };
            abs = toBox.Text.Split('\n');
            int pos = 0;
            for (int i = 0; i < abs.Count(); i++)
            {
                toBox.SelectionStart = pos;
                toBox.SelectionLength = abs[i].Length + 1;
                if (i % 2 == 0) { toBox.SelectionBackColor = Color.FromArgb(246, 233, 209); }
                else { toBox.SelectionBackColor = Color.FromArgb(216, 203, 177); }
                if (abs[i].Contains("killed")) { toBox.SelectionBackColor = Color.FromArgb(110, 255, 90); }
                pos += abs[i].Length + 1;
            }
        }
        public static void LoadScene(Form form)
        {
            form.Location = new Point(CurrentForm.Location.X, CurrentForm.Location.Y);
            form.Show();
            CurrentForm.Hide();
            CurrentForm = form;
        }
    }
    public static class DataStorage
    {        
        public static string LatestLog;
        public static int ArenaLvl { get; set; }
        public static string Nick { get; set; }        
        public static void UpdateDB()
        {
            DB db = new DB();
            MySqlCommand command = new MySqlCommand($"UPDATE characters SET `level` = {UnitsHandler.Player.Lvl}, `experience`={UnitsHandler.Player.XP},`attributepoints`={UnitsHandler.Player.Points},`gold`={UnitsHandler.Player.Gold},`hp`={UnitsHandler.Player.MaxHP},`str`={UnitsHandler.Player.Str},`agi`={UnitsHandler.Player.Agi},`con`={UnitsHandler.Player.Con} WHERE `name` ='{DataStorage.Nick}'", db.GetConnection());
            db.OpenConnection();
            command.ExecuteNonQuery();
            db.CloseConnection();
        }
        public static int GetKillGoldBounty()
        {
            int amount;
            amount = Convert.ToInt32(2 * Math.Pow(2, DataStorage.ArenaLvl));
            int rand = Dice.Next(Convert.ToInt32(Math.Round(amount * 0.3)));
            if (Dice.Next() > 50) rand = rand * -1;
            amount += rand;
            return amount;
        }
        public static int GetKillXPBounty()
        {
            int amount;
            amount = Convert.ToInt32(6 * Math.Pow(2, DataStorage.ArenaLvl));
            int rand = Dice.Next(Convert.ToInt32(Math.Round(amount * 0.2)));
            if (Dice.Next() > 50) rand = rand * -1;
            amount += rand;
            return amount;
        }
    }
}
